# Bitovi Monitoring
## Description
This repository utilizes helm to create resources in Kubernetes. Both Prometheus and Grafana are deployed with all necessary configuration managed in ConfigMaps, including the dashboard and alerting.
